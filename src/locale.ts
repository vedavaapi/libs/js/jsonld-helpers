import { IText, from as textFrom } from '@vedavaapi/types/dist/Text';

import { kvListToMap } from './structure';


export function getLocalizedText({ textOptions, locale, fallbackLocales }: {
    textOptions?: IText | IText[] | string,
    locale?: string;
    fallbackLocales?: string[]
}): IText | undefined {
    if (textOptions === undefined || textOptions === null) return undefined;
    // eslint-disable-next-line no-nested-ternary
    const nTextOptions: IText[] = (typeof textOptions === 'string') ? [textFrom({ chars: textOptions })] : (Array.isArray(textOptions) ? textOptions : [textOptions]);
    if (!nTextOptions.length) return undefined;
    if (!locale) return nTextOptions[0];

    const localeTextMap = kvListToMap(nTextOptions, 'language');
    let matchedText: IText | undefined = localeTextMap[locale];
    if (matchedText || !fallbackLocales) return matchedText;

    fallbackLocales.some((fLocale) => {
        const fMatchedText = localeTextMap[fLocale];
        if (fMatchedText) matchedText = fMatchedText;
        return !!fMatchedText;
    });
    if (matchedText || !fallbackLocales.includes('*')) return matchedText;

    return nTextOptions[0];
}


export class LocaleContext {
    public locale?: string;

    public fallbackLocales?: string[];

    constructor({ locale, fallbackLocales }: {
        locale?: string;
        fallbackLocales?: string[];
    }) {
        this.locale = locale;
        this.fallbackLocales = fallbackLocales;
    }

    public getLocalizedText(textOptions?: IText | IText[] | string) {
        return getLocalizedText({
            textOptions, locale: this.locale, fallbackLocales: this.fallbackLocales,
        });
    }
}
