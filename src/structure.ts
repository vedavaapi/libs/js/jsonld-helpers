import { IPlainObject } from '@vedavaapi/types/dist/base';


export function kvListToMap(list: IPlainObject[], kf: string, vf?: string) {
    const map: IPlainObject = {};
    list.forEach((item) => {
        map[item[kf]] = vf ? item[vf] : item;
    });
    return map;
}
